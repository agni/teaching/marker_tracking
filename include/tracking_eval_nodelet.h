#pragma once

#include <vector>
//ros
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Vector3.h>

//messages
#include <geometry_msgs/TransformStamped.h>
#include <tf2_msgs/TFMessage.h>

#include <std_msgs/Float64.h>

namespace aruco_tracking {

    class EvalNodelet : public nodelet::Nodelet {
    private:
        ros::Subscriber pos_sub_;
        std::vector<ros::Publisher> jitter_pos_pubs_ = std::vector<ros::Publisher>();
        std::vector<ros::Publisher> jitter_ort_pubs_ = std::vector<ros::Publisher>();
        std::vector<ros::Publisher> variation_pos_pubs_ = std::vector<ros::Publisher>();
        std::vector<ros::Publisher> variation_ort_pubs_ = std::vector<ros::Publisher>();

        std::vector<std::string> tracker_names;

        const size_t history_size = 50;

        tf2::Vector3 last_pos;
        tf2::Vector3 last_slope;
        std::vector<double> angle_history;
        size_t angle_history_index = 0;

        tf2::Quaternion last_ort;
        tf2::Quaternion last_ort_slope;
        std::vector<double> angle_ort_history;
        size_t angle_ort_history_index = 0;

        std::vector<tf2::Vector3> pos_history;
        tf2::Vector3 history_sum;
        size_t pos_history_index = 0;

        std::vector<tf2::Quaternion> ort_history;
        size_t ort_history_index = 0;

        void onInit() override;

        void posCB(const tf2_msgs::TFMessageConstPtr &pos_msg);

        std_msgs::Float64 jitter_pos(const tf2::Vector3 &pos);

        std_msgs::Float64 jitter_ort(const tf2::Quaternion &ort);

        std_msgs::Float64 variation_pos(const tf2::Vector3 &pos);

        std_msgs::Float64 variation_ort(const tf2::Quaternion &ort);
    };

}
