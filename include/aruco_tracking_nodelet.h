#pragma once

//ros
#include <ros/ros.h>
#include <nodelet/nodelet.h>
#include <cv_bridge/cv_bridge.h>
//messages
#include <sensor_msgs/Image.h>
#include <sensor_msgs/CameraInfo.h>
#include <geometry_msgs/Pose.h>
//aruco
#include <aruco_custom.h>
//dynamic reconfigure
#include <aruco_tracking/ArucoMarkerConfig.h>
#include <dynamic_reconfigure/server.h>
#include <unordered_map>

namespace aruco_tracking {

    class Nodelet : public nodelet::Nodelet {
    private:
        std::string cam_id_;
        ros::Subscriber cam_info_sub_;
        ros::Subscriber image_sub_;
        ros::Publisher image_pub_;
        ros::Publisher marker_pub_;

        std::unordered_map<std::string, ros::Publisher> debugImages;

        cv::Mat camMatrix_, distCoeffs_;

        boost::recursive_mutex config_mutex;
        aruco_tracking::ArucoMarkerConfig m_config{};
        std::unique_ptr<dynamic_reconfigure::Server<aruco_tracking::ArucoMarkerConfig>> server{};

        cv::Ptr <cv::aruco::DetectorParameters> detectorParams_;
        cv::Ptr <cv::aruco::Dictionary> dictionary_;
        double marker_side_length_{};
        std::map<std::string, cv::Ptr<cv::aruco::Board>> boards_;

        void onInit() override;
        void createBoards(ros::NodeHandle nh);

        void infoCB(const sensor_msgs::CameraInfoConstPtr &info_msg);

        void imageCB(const sensor_msgs::ImageConstPtr &image_msg);

        void findAndPublishMarkers(const cv_bridge::CvImagePtr& cv_ptr);

        void reconfigureCallback(const aruco_tracking::ArucoMarkerConfig& config, uint32_t /*level*/);

        void updateConfig(int dict_id);

        geometry_msgs::Pose getPose(const cv::Vec3d &rvec, const cv::Vec3d &tvec);
    };

}//namespace
