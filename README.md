This code realizes object tracking with multiple cameras based on OpenCV' marker tracking (Aruco markers)

# Build instructions
- Install ROS Noetic
  - on Ubuntu 20.04 following the [official instructions](http://wiki.ros.org/noetic/Installation)
  - on Windows 10/11 [using WSL2](https://github.com/rhaschke/lecture/wiki/WSL-install)
- Install catkin tools
	```sh
	sudo apt install python3-catkin-tools python3-osrf-pycommon
	```
- Create a catkin workspace for ROS
	```sh
	mkdir -p ~/ros/src
	cd ~/ros
	catkin config --extend /opt/ros/noetic
	cd src
	git clone https://gitlab.ub.uni-bielefeld.de/agni/teaching/marker_tracking
	```
- Install dependencies
	```sh
	rosdep install -y --from-paths . --ignore-src --rosdistro noetic
	```
- Build your workspace
	```sh
	catkin build
	```

# Usage instructions
- Source your workspace
	```sh
	source devel/setup.bash
	```
- Launch the example
	```sh
	roslaunch aruco_tracking aruco_tracking.launch
	```
- Replay rosbag files from [sciebo](https://uni-bielefeld.sciebo.de/s/eIQHW9IInkijgu3):
	```sh
	rosbag play -l large-motions.bag
	```

# Relevant Reads
- [OpenCV: Detection of markers](https://docs.opencv.org/4.x/d5/dae/tutorial_aruco_detection.html)
- [OpenCV: Detection of marker boards](https://docs.opencv.org/4.x/db/da9/tutorial_aruco_board_detection.html)  
	[`estimatePoseBoard()`](https://github.dev/opencv/opencv_contrib/blob/c52e7fcf652067c43874943afaeb01c3cc4df874/modules/aruco/src/aruco.cpp#L1564-L1583) calls [`solvePnP()`](https://github.dev/opencv/opencv/blob/c8228e5789510ec26378eceb17311dd7bddf0b33/modules/calib3d/include/opencv2/calib3d.hpp#L959-L963) which uses `SOLVEPNP_ITERATIVE` as its default solver,
	which in turn calls [`cvFindExtrinsicCameraParams2()`](https://github.dev/opencv/opencv/blob/c8228e5789510ec26378eceb17311dd7bddf0b33/modules/calib3d/src/solvepnp.cpp#L906-L908), which is based on the paper: [Z. Zhang. "A flexible new technique for camera calibration".
    *IEEE Transactions on Pattern Analysis and Machine Intelligence*, 22(11):1330-1334, 2000](https://uni-bielefeld.sciebo.de/f/454830357)
- [OpenCV: Perspective-n-Point (PnP) pose computation](https://docs.opencv.org/4.x/d5/d1f/calib3d_solvePnP.html) lists several alternative solvers:
	- [**`solvePnPRansac()`**](https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html#ga50620f0e26e02caa2e9adc07b5fbf24e) claims to be robust against outliers due to a [RANSAC approach](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.475.1243&rep=rep1&type=pdf)
	- [**`solvePnPRefineVVS()`**](https://docs.opencv.org/4.x/d9/d0c/group__calib3d.html#ga17491c0282e4af874f6206a9166774a5)
- Original Aruco Paper: [Garrido-Jurado, Muñoz-Salinas, Madrid-Cuevas, Marín-Jiménez. "Automatic generation and detection of highly reliable fiducial markers under occlusion". *Pattern Recognition*, 47:2280–2292, 2014](https://uni-bielefeld.sciebo.de/f/454830353)
- Cyrill Stachnis: [Camera Calibration Lecture](https://uni-bielefeld.sciebo.de/f/454834149)
