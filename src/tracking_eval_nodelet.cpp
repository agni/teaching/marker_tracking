#include <pluginlib/class_list_macros.h>
#include <tracking_eval_nodelet.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

namespace aruco_tracking {
    void EvalNodelet::onInit() {
        ros::NodeHandle nh = getNodeHandle();
        ros::NodeHandle np = getPrivateNodeHandle();

        if (!np.getParam("tracker_names", tracker_names)) {
            ROS_WARN(" Failed to fetch tracker names to evaluate.");
            return;
        }

        pos_sub_ = nh.subscribe(
                "/tf",
                1,
                &EvalNodelet::posCB,
                this);

        for (const auto &item: tracker_names) {
            jitter_pos_pubs_.push_back(nh.advertise<std_msgs::Float64>("jitter_pos_" + item, 1));
            jitter_ort_pubs_.push_back(nh.advertise<std_msgs::Float64>("jitter_ort_" + item, 1));
            variation_pos_pubs_.push_back(nh.advertise<std_msgs::Float64>("variation_pos_" + item, 1));
            variation_ort_pubs_.push_back(nh.advertise<std_msgs::Float64>("variation_ort_" + item, 1));
        }

        angle_history = std::vector<double>(history_size);
        last_pos = tf2::Vector3(1, 1, 1);
        last_slope = tf2::Vector3(1, 1, 1);

        angle_ort_history = std::vector<double>(history_size);
        last_ort = tf2::Quaternion(1, 0, 0, 0);
        last_ort_slope = tf2::Quaternion(1, 0, 0, 0);

        pos_history = std::vector<tf2::Vector3>(history_size);
        history_sum = tf2::Vector3(0, 0, 0);

        ort_history = std::vector<tf2::Quaternion>(history_size);
    }

    void EvalNodelet::posCB(const tf2_msgs::TFMessageConstPtr &pos_msg) {
        const std::vector<geometry_msgs::TransformStamped> transforms = pos_msg.get()->transforms;
        for (const auto & transform : transforms) {
            for (size_t i = 0; i < tracker_names.size(); ++i) {
                if (transform.child_frame_id == tracker_names[i]) {
                    tf2::Vector3 pos;
                    tf2::Quaternion ort;
                    tf2::convert(transform.transform.translation, pos);
                    tf2::convert(transform.transform.rotation, ort);

                    jitter_pos_pubs_[i].publish(jitter_pos(pos));
                    jitter_ort_pubs_[i].publish(jitter_ort(ort));
                    variation_pos_pubs_[i].publish(variation_pos(pos));
                    variation_ort_pubs_[i].publish(variation_ort(ort));
                }
            }
        }
    }

    std_msgs::Float64 EvalNodelet::jitter_pos(const tf2::Vector3 &pos) {
        tf2::Vector3 slope = pos - last_pos;
        angle_history[angle_history_index] = slope.angle(last_slope);
        last_pos = pos;
        last_slope = slope;
        angle_history_index = (angle_history_index + 1) % history_size;
        double average = 0;
        for (const auto &item: angle_history) {
            average += item;
        }
        average /= (double) history_size;
        std_msgs::Float64 msg;
        msg.data = average;
        return msg;
    }

    std_msgs::Float64 EvalNodelet::jitter_ort(const tf2::Quaternion &ort) {
        tf2::Quaternion slope = ort - last_ort;
        angle_ort_history[angle_ort_history_index] = slope.angleShortestPath(last_ort_slope);
        last_ort = ort;
        last_ort_slope = slope;
        angle_ort_history_index = (angle_ort_history_index + 1) % history_size;
        double average = 0;
        for (const auto &item: angle_ort_history) {
            average += item;
        }
        average /= (double) history_size;
        std_msgs::Float64 msg;
        msg.data = average;
        return msg;
    }

    std_msgs::Float64 EvalNodelet::variation_pos(const tf2::Vector3 &pos) {
        history_sum -= pos_history[pos_history_index];
        history_sum += pos;
        pos_history[pos_history_index] = pos;
        pos_history_index = (pos_history_index + 1) % history_size;
        tf2::Vector3 avg_pos = history_sum / (double) history_size;
        double average = 0;
        for (const auto &item: pos_history) {
            tf2::Vector3 delta_pos = item - avg_pos;
            average += delta_pos.length();
        }
        average /= (double) history_size;
        std_msgs::Float64 msg;
        msg.data = average;
        return msg;
    }

    std_msgs::Float64 EvalNodelet::variation_ort(const tf2::Quaternion &ort) {
        ort_history[ort_history_index] = ort;
        ort_history_index = (ort_history_index + 1) % history_size;

        //calculate quaternion avg
        tf2::Quaternion avg = ort_history[0];
        for (size_t i = 1; i < history_size; ++i) {
            avg = avg.slerp(ort_history[i], 1.0/((double)(i + 1)));
        }

        double average = 0;
        for (const auto &item : ort_history) {
            average += item.angleShortestPath(avg);
        }
        average /= (double) history_size;
        std_msgs::Float64 msg;
        msg.data = average;
        return msg;
    }
}

PLUGINLIB_EXPORT_CLASS(aruco_tracking::EvalNodelet, nodelet::Nodelet)
