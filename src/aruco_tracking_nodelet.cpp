#include <pluginlib/class_list_macros.h>
#include <opencv2/calib3d.hpp>
#include <aruco_tracking_nodelet.h>
#include <aruco_tracking/ArucoMarkers.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

namespace aruco_tracking {
    void Nodelet::onInit() {
        ros::NodeHandle nh = getNodeHandle();
        ros::NodeHandle np = getPrivateNodeHandle();

        int dictionary_id;
        if (!(np.getParam("cam_id", cam_id_) &&
              nh.getParam("aruco_dictionary_id", dictionary_id) &&
              nh.getParam("marker_side_length", marker_side_length_))) {
            ROS_ERROR(" Failed to fetch required parameters.");
            ros::shutdown();
        }

        detectorParams_ = cv::aruco::DetectorParameters::create();
        if (!(np.getParam("corner_refinement_method", detectorParams_->cornerRefinementMethod) &&
              np.getParam("corner_refinement_win_size", detectorParams_->cornerRefinementWinSize) &&
              np.getParam("corner_refinement_max_iterations", detectorParams_->cornerRefinementMaxIterations) &&
              np.getParam("corner_refinement_min_accuracy", detectorParams_->cornerRefinementMinAccuracy) &&
              np.getParam("error_correction_rate", detectorParams_->errorCorrectionRate) &&
              np.getParam("polygonalApproxAccuracyRate", detectorParams_->polygonalApproxAccuracyRate) &&
              np.getParam("minCornerDistanceRate", detectorParams_->minCornerDistanceRate) &&
              np.getParam("minDistanceToBorder", detectorParams_->minDistanceToBorder) &&
              np.getParam("minOtsuStdDev", detectorParams_->minOtsuStdDev) &&
              np.getParam("perspectiveRemovePixelPerCell", detectorParams_->perspectiveRemovePixelPerCell) &&
              np.getParam("perspectiveRemoveIgnoredMarginPerCell", detectorParams_->perspectiveRemoveIgnoredMarginPerCell))) {
            ROS_WARN(" Failed to fetch some marker detection parameters.");
        }

        //setup publishers/subscribers
        cam_info_sub_ = nh.subscribe("/" + cam_id_ + "/camera_info", 1, &Nodelet::infoCB, this);
        image_pub_ = nh.advertise<sensor_msgs::Image>("/" + cam_id_ + "/image_debug", 1);
        marker_pub_ = nh.advertise<aruco_tracking::ArucoMarkers>(nh.resolveName("markers"), 1);

        //setup dynamic reconfigure
        server = std::make_unique<dynamic_reconfigure::Server<aruco_tracking::ArucoMarkerConfig>>(config_mutex, ros::NodeHandle(cam_id_));
        updateConfig(dictionary_id);
        dynamic_reconfigure::Server<aruco_tracking::ArucoMarkerConfig>::CallbackType f;
        f = boost::bind(&Nodelet::reconfigureCallback, this, _1, _2);
        server->setCallback(f);

        dictionary_ = cv::aruco::getPredefinedDictionary(cv::aruco::PREDEFINED_DICTIONARY_NAME(dictionary_id));

        try {
            createBoards(nh);
        } catch (const std::exception &e) {
            std::cerr << e.what() << std::endl;
            throw;
        }
    }

    float extractFloat(XmlRpc::XmlRpcValue& v) {
        if (v.getType() == XmlRpc::XmlRpcValue::TypeDouble)
            return static_cast<double>(v);
        else if (v.getType() == XmlRpc::XmlRpcValue::TypeInt)
            return static_cast<int>(v);
        else
            return 0.0;
    }

    void Nodelet::createBoards(ros::NodeHandle nh) {
        const float s = marker_side_length_ / 2.;
        cv::Matx44f corners({-s,  s, 0, 1, // x y z 1
                              s,  s, 0, 1,
                              s, -s, 0, 1,
                             -s, -s, 0, 1});

        XmlRpc::XmlRpcValue boards;
        nh.getParam("boards", boards); // read board definitions from ROS parameter server
        // iterate over all boards
        for (auto bit=boards.begin(), bend=boards.end(); bit != bend; ++bit) {
            const std::string &name = bit->first;
            auto &markers = bit->second;

            std::vector<int> marker_ids;
            std::vector<cv::Mat> marker_points;

            // iterate over all markers of the board
            for (auto mit=markers.begin(), mend=markers.end(); mit != mend; ++mit) {
                marker_ids.push_back(std::stoi(mit->first));
                auto &vec = mit->second;
                cv::Vec3f tvec { extractFloat(vec[0]), extractFloat(vec[1]), extractFloat(vec[2]) };
                cv::Vec3f rvec { extractFloat(vec[3]), extractFloat(vec[4]), extractFloat(vec[5]) };
                cv::Mat T;
                cv::Rodrigues(rvec, T);
                cv::hconcat(T, tvec, T);
                marker_points.push_back(T * corners.t());
            }
            boards_.insert(std::make_pair(name, cv::aruco::Board::create(marker_points, dictionary_, marker_ids)));
        }
    }

    //get camera params and subscribe to the image
    void Nodelet::infoCB(const sensor_msgs::CameraInfoConstPtr &info_msg) {
        camMatrix_ = cv::Mat(3, 3, CV_64FC1, const_cast<double*>(info_msg->K.data())).clone();
        distCoeffs_ = cv::Mat(info_msg->D, true);

        cam_info_sub_.shutdown();
        image_sub_ = getNodeHandle().subscribe("/" + cam_id_ + "/image_raw", 10, &Nodelet::imageCB, this);
    }

    //convert sensor_msgs::Image to OpenCV::Mat; visualize image
    void Nodelet::imageCB(const sensor_msgs::ImageConstPtr &image_msg) {
        cv_bridge::CvImagePtr cv_ptr;
        try {
            // In order to yield correct coloring of aruco markers in image, we need to specify BGR instead of RGB?!?
            cv_ptr = cv_bridge::toCvCopy(image_msg, sensor_msgs::image_encodings::BGR8);
        }
        catch (cv_bridge::Exception &e) {
            ROS_ERROR("Not able to convert sensor_msgs::Image to OpenCV::Mat format %s", e.what());
            return;
        }

        // detect markers
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f>> corners, rejected;
        cv::aruco::detectMarkers(cv_ptr->image, debugImages, getNodeHandle(), cam_id_, dictionary_, corners, ids, detectorParams_, rejected);
        cv::aruco::drawDetectedMarkers(cv_ptr->image, corners, ids); // debug image

        // estimate board pose(s)
        cv::Vec3d rvec, tvec;
        for (const auto& pair : boards_) {
            try {
            //std::cerr << cam_id_ << ":" << pair.first << ": ";
            if (cv::aruco::estimatePoseBoard(corners, ids, pair.second, camMatrix_, distCoeffs_, rvec, tvec))
                ;//std::cerr << tvec << " " << rvec;
            else
                std::cerr << "failure";
            } catch (const std::exception &e) {
                std::cerr << e.what();
            }
            //std::cerr << std::endl;
        }

        // compute marker poses
        std::vector <cv::Vec3d> rvecs, tvecs;
        cv::aruco::estimatePoseSingleMarkers(corners, marker_side_length_, camMatrix_, distCoeffs_, rvecs, tvecs);

        // process all boards and publish their pose w.r.t. the camera
        ArucoMarkers markers;
        markers.header.frame_id = cam_id_;
        markers.header.stamp = ros::Time::now(); // cv_ptr->header.stamp;
        markers.ids = ids;
        markers.poses.reserve(ids.size());
        for (int i = 0; i < ids.size(); i++) {
            cv::drawFrameAxes(cv_ptr->image, camMatrix_, distCoeffs_, rvecs[i], tvecs[i], 0.05);
            markers.poses.emplace_back(getPose(rvecs[i], tvecs[i]));
        }

        cv_ptr->header.stamp = markers.header.stamp;
        image_pub_.publish(cv_ptr);  // re-publish augmented image
        marker_pub_.publish(markers);
    }

    geometry_msgs::Pose Nodelet::getPose(const cv::Vec3d &rvec, const cv::Vec3d &tvec) {
        double angle = cv::norm(rvec);
        cv::Vec3d axis = rvec / angle;

        geometry_msgs::Pose pose;
        auto& pos = pose.position;
        pos.x = tvec[0];
        pos.y = tvec[1];
        pos.z = tvec[2];
        tf2::convert(tf2::Quaternion(tf2::Vector3(axis[0], axis[1], axis[2]), angle), pose.orientation);
        return pose;
    }

    void Nodelet::reconfigureCallback(const aruco_tracking::ArucoMarkerConfig &config, uint32_t) {
        m_config = config;

        detectorParams_->detectInvertedMarker = config.detectInvertedMarker;

        dictionary_ = cv::aruco::getPredefinedDictionary(config.marker_type);
        detectorParams_->adaptiveThreshConstant = config.adaptiveThreshConstant;
        detectorParams_->adaptiveThreshWinSizeMin = config.adaptiveThreshWinSizeMin;
        detectorParams_->adaptiveThreshWinSizeMax = config.adaptiveThreshWinSizeMax;
        detectorParams_->adaptiveThreshWinSizeStep = config.adaptiveThreshWinSizeStep;
        detectorParams_->cornerRefinementMaxIterations = config.cornerRefinementMaxIterations;
        detectorParams_->cornerRefinementMinAccuracy = config.cornerRefinementMinAccuracy;
        detectorParams_->cornerRefinementWinSize = config.cornerRefinementWinSize;

        detectorParams_->errorCorrectionRate = config.errorCorrectionRate;
        detectorParams_->minCornerDistanceRate = config.minCornerDistanceRate;
        detectorParams_->markerBorderBits = config.markerBorderBits;
        detectorParams_->maxErroneousBitsInBorderRate = config.maxErroneousBitsInBorderRate;
        detectorParams_->minDistanceToBorder = config.minDistanceToBorder;
        detectorParams_->minMarkerDistanceRate = config.minMarkerDistanceRate;
        detectorParams_->minMarkerPerimeterRate = config.minMarkerPerimeterRate;
        detectorParams_->maxMarkerPerimeterRate = config.maxMarkerPerimeterRate;
        detectorParams_->minOtsuStdDev = config.minOtsuStdDev;
        detectorParams_->perspectiveRemoveIgnoredMarginPerCell = config.perspectiveRemoveIgnoredMarginPerCell;
        detectorParams_->perspectiveRemovePixelPerCell = config.perspectiveRemovePixelPerCell;
        detectorParams_->polygonalApproxAccuracyRate = config.polygonalApproxAccuracyRate;

        detectorParams_->markerCandidateIndex = config.markerCandidateIndex;
    }

    void Nodelet::updateConfig(const int dict_id) {
        boost::recursive_mutex::scoped_lock lock(config_mutex);

        m_config.marker_type = dict_id;
        m_config.adaptiveThreshConstant = detectorParams_->adaptiveThreshConstant;
        m_config.adaptiveThreshWinSizeMin = detectorParams_->adaptiveThreshWinSizeMin;
        m_config.adaptiveThreshWinSizeMax = detectorParams_->adaptiveThreshWinSizeMax;
        m_config.adaptiveThreshWinSizeStep = detectorParams_->adaptiveThreshWinSizeStep;
        m_config.cornerRefinementMaxIterations = detectorParams_->cornerRefinementMaxIterations;
        m_config.cornerRefinementMinAccuracy = detectorParams_->cornerRefinementMinAccuracy;
        m_config.cornerRefinementWinSize = detectorParams_->cornerRefinementWinSize;

        m_config.errorCorrectionRate = detectorParams_->errorCorrectionRate;
        m_config.minCornerDistanceRate = detectorParams_->minCornerDistanceRate;
        m_config.markerBorderBits = detectorParams_->markerBorderBits;
        m_config.maxErroneousBitsInBorderRate = detectorParams_->maxErroneousBitsInBorderRate;
        m_config.minDistanceToBorder = detectorParams_->minDistanceToBorder;
        m_config.minMarkerDistanceRate = detectorParams_->minMarkerDistanceRate;
        m_config.minMarkerPerimeterRate = detectorParams_->minMarkerPerimeterRate;
        m_config.maxMarkerPerimeterRate = detectorParams_->maxMarkerPerimeterRate;
        m_config.minOtsuStdDev = detectorParams_->minOtsuStdDev;
        m_config.perspectiveRemoveIgnoredMarginPerCell = detectorParams_->perspectiveRemoveIgnoredMarginPerCell;
        m_config.perspectiveRemovePixelPerCell = detectorParams_->perspectiveRemovePixelPerCell;
        m_config.polygonalApproxAccuracyRate = detectorParams_->polygonalApproxAccuracyRate;

        server->updateConfig(m_config);
    }

}//namespace

PLUGINLIB_EXPORT_CLASS(aruco_tracking::Nodelet, nodelet::Nodelet);
