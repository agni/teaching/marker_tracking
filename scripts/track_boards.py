#!/usr/bin/env python
from __future__ import print_function

import numpy
import rospy
import tf2_ros
from tf import transformations as tf
from geometry_msgs.msg import Pose, TransformStamped, Vector3, Quaternion
from std_msgs.msg import ColorRGBA
from visualization_msgs.msg import Marker
from aruco_tracking.msg import ArucoMarkers


def inverse(T):
    return numpy.linalg.inv(T)


def interpolate(qp1, qp2, scale):
    """Interpolate between two transforms, represented as (quaternion, position) tuples each"""
    q = tf.quaternion_slerp(qp1[0], qp2[0], scale)
    p = scale * qp1[1] + (1-scale) * qp2[1]
    return q, p


def qp_from_matrix(T):
    return tf.quaternion_from_matrix(T), T[0:3, 3]


def matrix_from_qp(qp):
    """Retrieve matrix from (quaternion, position) tuple"""
    q, p = qp
    T = tf.quaternion_matrix(q)
    T[0:3, 3] = p
    return T


def pose_to_T(msg: Pose):
    """Transform Pose message into homogeneous transform"""
    q = msg.orientation
    p = msg.position
    T = tf.quaternion_matrix([q.x, q.y, q.z, q.w])
    T[0:3, 3] = numpy.array([p.x, p.y, p.z])
    return T


class BoardTracker(object):
    """Track a set of aruco markers combined on a rigid object"""

    def __init__(self, name):
        self.name = name
        self.markers = dict()
        self.T = None
        markers = rospy.get_param('boards/{name}'.format(name=name), dict())
        # convert transform from position, axis*angle representation to (inverse) 4x4 matrix
        for m in markers.keys():
            pos = numpy.array(markers[m][:3])
            axis = numpy.array(markers[m][3:])
            angle = numpy.linalg.norm(axis)
            if angle < 1e-6:
                axis = [0, 0, 1]
            T = tf.rotation_matrix(angle, axis)
            T[0:3, 3] = pos
            self.markers[int(m)] = inverse(T)

    def transforms(self, msg: ArucoMarkers):
        """Get (estimated) transforms of board within the frame msg.header.frame_id
           from aruco marker transforms w.r.t. that frame, i.e. frame_id -> marker -> board"""
        return numpy.asarray([pose_to_T(pose).dot(self.markers[id]) for id, pose in zip(msg.ids, msg.poses) if id in self.markers.keys()])

    @classmethod
    def _average(cls, Ts):
        if Ts.shape[0] == 0:
            return None  # no matrix at all!
        if Ts.shape[0] == 1:
            return Ts[0]  # only a single matrix
        qp = qp_from_matrix(Ts[0])
        n = 1
        for T in Ts[1:]:  # process remaining Ts
            n += 1
            qp = interpolate(qp, qp_from_matrix(T), 1/n)
        return matrix_from_qp(qp)


class CameraTrackers(BoardTracker):
    def __init__(self, name):
        super().__init__(name)
        self.cams = dict()  # additionally store last transform

    def average(self, Ts, camera):
        if Ts.shape[0] == 0:
            # return previous camera pose (if available)
            qp = self.cams.get(camera, None)
            return matrix_from_qp(qp) if qp is not None else None

        T = BoardTracker._average(Ts)  # new transform estimate
        qp = qp_from_matrix(T)
        if camera in self.cams:
            # compute sliding average for more robuts camera pose estimation
            qp = interpolate(self.cams[camera], qp, 0.9)
            T = matrix_from_qp(qp)

        self.cams[camera] = qp
        return T


class Tracking(object):
    def __init__(self, marker_list, reference_frame='world'):
        self.world = CameraTrackers(reference_frame)

        self.boards = [BoardTracker(name) for name in marker_list]
        self.board_markers = dict()  # store marker transforms received from individual cameras
        self.marker_sub = rospy.Subscriber('markers', ArucoMarkers, self.marker_cb)
        self.tf_broadcaster = tf2_ros.TransformBroadcaster()

    def marker_cb(self, msg: ArucoMarkers):
        # estimate camera pose in world
        T = self.world.average(self.world.transforms(msg), msg.header.frame_id)
        if T is None:
            return
        T = inverse(T)
        self.publish(self.world.name, msg.header.frame_id, T, msg.header.stamp)

        for board in self.boards:
            try:
                # estimate board pose in world from world -> camera -> marker -> board
                Ts = numpy.matmul(T, board.transforms(msg))

                # for debugging: marker frames
                for id, pose in zip(msg.ids, msg.poses):
                    if id in board.markers.keys():
                        self.publish(msg.header.frame_id, '{cam}-{id}'.format(cam=msg.header.frame_id, id=id),
                                     pose_to_T(pose), msg.header.stamp)
                # for debugging: publish board frame estimates
                for T, id in zip(Ts, [id for id in msg.ids if id in board.markers.keys()]):
                    self.publish(self.world.name, '{board}-{cam}-{id}'.format(board=board.name, cam=msg.header.frame_id, id=id),
                                 T, msg.header.stamp)
            except ValueError:  # handle empty list (no markers recognized)
                continue

            # update board positions in board_markers buffer
            markers = self.board_markers.get(board.name, dict())
            markers.update([(msg.header.frame_id, Ts)])
            self.board_markers[board.name] = markers

            # average over board estimates from all cameras and markers
            M = BoardTracker._average(numpy.concatenate(list(markers.values())))
            self.publish(self.world.name, board.name, M, msg.header.stamp)

    def publish(self, parent, child, T, stamp):
        t = TransformStamped()
        t.header.stamp = stamp
        t.header.frame_id = parent
        t.child_frame_id = child
        vec = T[0:3, 3]
        t.transform.translation = Vector3(*vec)
        quart = tf.quaternion_from_matrix(T)
        t.transform.rotation = Quaternion(*quart)
        self.tf_broadcaster.sendTransform(t)


rospy.init_node('track_boards')
tracking = Tracking(['cube'])

# publish cube marker once
marker_pub = rospy.Publisher('marker', Marker, queue_size=1, latch=True)
m = Marker(type=Marker.CUBE, scale=Vector3(0.04, 0.04, 0.04), color=ColorRGBA(1, 1, 1, 0.5))
m.header.frame_id = 'cube'
m.pose.orientation.w = 1.0
m.frame_locked = True
marker_pub.publish(m)

rospy.spin()
