#!/usr/bin/env python
from __future__ import print_function

import numpy
from tf import transformations as tf

numpy.set_printoptions(suppress=True, linewidth=120)

### script to compute marker transforms, given a board definition ###

M = 0.0375 / 2
C = 0.04 / 2
# marker corners in xy plane of base frame
src = M * numpy.array([[-1, 1], [1, 1], [1, -1], [-1, -1]])
# augment z=0 component and homogeneous 1
src = numpy.vstack([src.T, numpy.zeros(4), numpy.ones(4)])

# marker corners in 3D ("cube definition")
dst = numpy.array([[[-M, -C, -M],
                    [-M, -C, +M],
                    [+M, -C, +M],
                    [+M, -C, -M]],  # marker 1
                   [[+C, -M, +M],
                    [+C, +M, +M],
                    [+C, +M, -M],
                    [+C, -M, -M]],  # marker 2
                   [[+M, +C, +M],
                    [-M, +C, +M],
                    [-M, +C, -M],
                    [+M, +C, -M]],  # marker 3
                   [[-C, +M, -M],
                    [-C, +M, +M],
                    [-C, -M, +M],
                    [-C, -M, -M]],  # marker 4
                   [[-M, -M, +C],
                    [-M, +M, +C],
                    [+M, +M, +C],
                    [+M, -M, +C]]])  # marker 5
# transpose individual marker matrices (swapaxes) and augment homogeneous 1
dst = numpy.concatenate([dst.swapaxes(1, 2), numpy.ones(4*dst.shape[0]).reshape(-1, 1, 4)], axis=1)

# compute transforms for all markers, solving dst = T * src
Ts = numpy.matmul(dst, numpy.linalg.pinv(src))
assert(numpy.allclose(dst, numpy.matmul(Ts, src)))
# augment missing z axes = cross products of x and y columns
Ts[:, :3, 2] = numpy.cross(Ts[:, :3, 0], Ts[:, :3, 1])
assert(numpy.allclose(dst, numpy.matmul(Ts, src)))

# print homogeneous transform matrices for all markers
print(Ts)

# print transforms as tvec, angle, axis triples (OpenCV expects rvec = angle * axis)
for i in range(Ts.shape[0]):
    T = numpy.array(Ts[i], copy=True)
    T[:3, 3] = numpy.zeros(3)
    angle, axis, _ = tf.rotation_from_matrix(T)
    assert(tf.is_same_transform(T, tf.rotation_matrix(angle, axis)))
    print('marker', i+1, Ts[i, :3, 3], angle*axis)
